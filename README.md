# About this site

For the most part this website has been built from scratch. It&rsquo;s a great way to learn how a website works &lsquo;under the hood&rsquo;, but it also helps me keep the website as clean as I can; notably, not a single <code>&lt;div&gt;</code> has been used in the making of this website, and W3C&rsquo;s unusually strict <a href="https://validator.w3.org/">Markup Validation Service</a> shows zero errors or warnings on this page.

Generally, I aim to strive for simplicity and minimalism wherever I can. I avoid JavaScript unless I deem it truly necessary. At present, this website only runs <a href="https://highlightjs.org/">highlight.js</a> (for syntax highlighting) and <a href="https://katex.org/">KaTeX</a> (for math typesetting).

One violation of this minimalist principle is that I use a custom webfont, specifically <a href="https://fonts.google.com/specimen/Roboto+Slab">Roboto Slab</a>. I couldn&rsquo;t resist!

The SVG buttons used at the top of the page are based on <a href="https://fontawesome.com/">Font Awesome</a> and are (modulo some modifications) taken from <a href="https://github.com/Rush/Font-Awesome-SVG-PNG">this repo</a>. The buttons&rsquo; background colour changes when you hover over them. Curiously, no JavaScript is needed for that; it can be done entirely in CSS using the <code>:hover</code> selector and the <code>fill</code> property.
