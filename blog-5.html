<!DOCTYPE html>
<html lang="en-US">
  <head>
    <!--Metadata-->
    <meta charset="utf-8">
    <meta name="author" content="Jeroen van der Meer">
    <title>Why n(n+1)/2 is an integer</title>
    <!--Stylesheet-->
    <link rel="stylesheet" href="style.css">
    <!--Mobile-friendliness-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Syntax highlighting-->
    <link rel="stylesheet" href="highlight.css">
    <script src="js/highlight.js"></script>
    <script>
      hljs.highlightAll();
    </script>
    <!--LaTeX rendering-->
    <link rel="stylesheet" href="katex.css">
    <script src="js/katex.js"></script>
    <script src="js/katex-auto-render.js"></script>
    <script>
      document.addEventListener("DOMContentLoaded", function () {
        renderMathInElement(document.body, {
          delimiters: [
            { left: "$", right: "$", display: false },
            { left: "\\[", right: "\\]", display: true },
          ],
          throwOnError: false,
        });
      });
    </script>
  </head>
  <body>
    <header>
      <nav>
        <p>
          <a href="blog-4.html">&larr;&nbsp;Previous</a>&nbsp;&verbar;&nbsp;<a
            href="index.html"
            >Home</a
          >&nbsp;&verbar;&nbsp;<a href="blog-6.html">Next&nbsp;&rarr;</a>
        </p>
        <hr>
      </nav>
    </header>
    <main>
      <h2>Why $n(n+1) / 2$ is an integer</h2>
      <p>
        Because either $n$ or $n + 1$ is even, hence $n(n+1)$ is always
        divisible by $2$.
      </p>
      <p>
        So that settles it, then. Or does it? Let&rsquo;s squint a bit and
        stretch what we mean by &lsquo;integer&rsquo;. Then a number theorist
        will inevitably find himself asking the following question: Let $K$ be a
        number field with ring of integers $\mathcal{O}_K$. Under what
        circumstances will the polynomial $f(x) = x(x+1) / 2$ preserve
        $\mathcal{O}_K$? The goal of this post will be to explore this question.
      </p>
      <p>
        The only relevant prerequisite is a background in algebraic number
        theory, say at the level of the (fantastic) book
        <i>A Classical Introduction to Modern Number Theory</i> by
        Ireland&ndash;Rosen.
      </p>
      <h3>A curious quotient</h3>
      <p>
        Given $x \in \mathcal{O}_K$, the product $x(x+1)$ remains in
        $\mathcal{O}_K$, because $\mathcal{O}_K$ is a ring. The quotient $x(x+1)
        / 2$ is surely well-defined in $K$, but it is no longer clear whether it
        is in $\mathcal{O}_K$. Almost tautologically, this will be the case if
        and only if $x(x+1)$ is a multiple of $2$ in $\mathcal{O}_K$.
        Equivalently, what we&rsquo;re saying is that $x(x+1)$ is equivalent to
        $0$ modulo $2$. This suggests we should take a look at $\mathcal{O}_K /
        (2)$. Which brings us to our first result:
      </p>
      <p>
        <b>Lemma 1.</b> Let $K$ be a number field. Then $f(x) = x(x+1) / 2$
        preserves algebraic integers if and only if $\mathcal{O}_K / (2)$ is
        isomorphic to a direct product $\mathbb{F}_2 \times \cdots \times
        \mathbb{F}_2$.
      </p>
      <p>
        <i>Proof:</i> In order for $x(x+1) / 2$ to be an algebraic integer, $x(x
        + 1)$ must be equivalent to $0$ modulo $2$. Equivalently, the quotient
        ring $\mathcal{O}_K / (2)$ must satisfy the identity $x(x+1) = 0$ for
        every $x$. In other words, $\mathcal{O}_K / (2)$ must be a Boolean ring.
        Notice moreover that the ring must be finite, the order being equal to
        the norm $N_{K/\mathbb{Q}}(2)$.
      </p>
      <p>
        I claim that the only finite Boolean rings are direct products
        $\mathbb{F}_2 \times \cdots \times \mathbb{F}_2$. In a Boolean ring,
        every element is idempotent. Idempotents correspond to splittings, in
        that if $e$ is an idempotent, then $R$ splits as $eR \oplus (1-e)R$. We
        can use this to inductively split up Boolean rings. But since our
        Boolean ring is finite, this process halts, at which point we reach the
        desired identification. &#8718;
      </p>
      <p>
        Alright, so what ring is $\mathcal{O}_K / (2)$? For the purposes of
        future sections, we might as well ask this in a more general setting.
        Let $L / K$ be an extension of number fields, and let $\mathfrak{p}$ be
        a prime in $K$. What is the quotient $\mathcal{O}_L / \mathfrak{p}$?
      </p>
      <p>
        The prime $\mathfrak{p}$, viewed as an ideal in $L$ splits as a product
        $\mathfrak{P}_1^{e_1} \cdots \mathfrak{P}_r^{e_r}$, where the $e_i$ are
        ramification indices. The Chinese remainder theorem lets us see that \[
        \mathcal{O}_L / \mathfrak{p} \cong \prod_{i=1}^r \mathcal{O}_L /
        \mathfrak{P}_i^{e_i} \text{.} \] We may thus focus our attention to a
        single prime $\mathfrak{P}$ lying over $\mathfrak{p}$. Now, we know that
        $\mathfrak{O}_{L} / \mathfrak{P} \cong \mathbb{F}_{p^f}$ where $f$ is
        the inertia degree of $\mathfrak{P}$, so if $e = 1$, then we are done.
        But what if we have ramification?
      </p>
      <p>
        <b>Lemma 2.</b> With the notation as above, we have \[\mathcal{O}_L /
        \mathfrak{P}^e \cong \mathbb{F}_{p^f}[\varepsilon] /
        (\varepsilon^e)\text{.}\]
      </p>
      <p>
        <i>Proof:</i> If $\mathcal{O}_{\mathfrak{P}}$ is the ring of integers of
        the completion $L_{\mathfrak{P}}$, then \[ \mathcal{O}_{\mathfrak{P}} /
        \mathfrak{P}^e \cong \mathcal{O}_L / \mathfrak{P}^e \text{,} \] so we
        may as well work locally. Write $L'$ for the maximal unramified
        extension of $\mathbb{Q}_p$ inside $L$. Then $L = L'(\pi)$ for a prime
        $\pi$, and $\mathfrak{P} = (\pi)$. Denote by $\mathfrak{p}$ the unique
        prime in $L'$ below $\mathfrak{P}$. Since $L / L'$ is totally ramified,
        the minimal polynomial of $\pi$ is an Eisenstein polynomial $f(x) \in
        L'[x]$ of degree $e$.
      </p>
      <p>
        Consider the evaluation map $\mathcal{O}_{L'}[x] \to \mathcal{O}_L$
        sending $g$ to $g(\pi)$ and factor it into a map \[ \mathcal{O}_{L'}[x]
        / (f) \to \mathcal{O}_L / \mathfrak{P}^e \text{.} \] This map is
        surjective, and its kernel is $\mathfrak{p} \mathcal{O}_{L'}$, hence
        \[\begin{split} \mathcal{O}_{L} / \mathfrak{P}^e &\cong
        (\mathcal{O}_{L'} / \mathfrak{p})[x] / (f) \\ &\cong \mathbb{F}_{p^f}[x]
        / (f) \end{split}\] but now since $f$ was an Eisenstein polynomial, it
        is equivalent to $x^e$ mod $p$, hence we get the desired result. &#8718;
      </p>
      <p>
        <b>Corollary 3.</b> Let $K$ be a number field. Then $f(x) = x(x+1)/2$
        preserves algebraic integers in $K$ if and only if $(2)$ splits
        completely in $K$.
      </p>
      <p><i>Proof:</i> Combine Lemmas 1 and 2. &#8718;</p>
      <p>
        <b>Example 4.</b> Consider a quadratic field $K = \mathbb{Q}(\sqrt{m})$
        with $m$ squarefree. The prime $(2)$ splits completely if and only if $m
        \equiv 1$ mod $8$, in which case an integral basis of $\mathcal{O}_K$ is
        given by $\big\{1,(\sqrt{m}-1)/2\big\}$. Indeed it&rsquo;s easily
        checked by hand that $f(x)$ preserves this basis.
      </p>
      <h3>An evident extension</h3>
      <p>
        Instead of looking at the product $x(x+1)$, let&rsquo;s take a look at
        $x(x+1)(x+2)$. It&rsquo;s easy to see that this product is divisible by
        $6$, and so $f(x) = x(x+1)(x+2) / 6$ is integer-valued. We can ask the
        same question as before, and by the same logic, we know that $f(x)$
        preserves $\mathcal{O}_K$ if and only if $x(x+1)(x+2)$ is identically
        $0$ in the ring $\mathcal{O}_K / (6)$. Now, unlike before, it is no
        longer clear to me whether finite rings with this property can easily be
        classified &mdash; though I suspect that they can.
      </p>
      <p>
        Luckily, we don&rsquo;t need to find a general classification. By the
        Chinese remainder theorem, it suffices to look at the quotients
        $\mathcal{O}_K / \mathfrak{P}^e$ where $\mathfrak{P}$ is now a prime
        lying either over $2$ or over $3$, and where $e$ is the ramification
        index of $\mathfrak{P}$ over $p$. We have the following result:
      </p>
      <p>
        <b>Lemma 5.</b> The polynomial $g_d(x) = x(x+1)(x+2)\cdots(x+d-1)$ is
        identically zero in the ring $\mathbb{F}_{p^f}[\varepsilon] /
        (\varepsilon^e)$ if and only if $f = 1$ and $e \leq d/p$.
      </p>
      <p>
        Before formulating this lemma, I wrote up some rough Sage code to check
        if I could find any pattern. The script is quite slow, but we&rsquo;ll
        improve it in a future iteration further down this post.
      </p>
      <pre><code class="language-python">def is_zero(d, p, f, e):
    """Given a polynomial g_d(x) = x(x+1)...(x+d-1), test if g_d(x) vanishes everywhere on the ring Q = F_{p^f}[x] / (x^e)."""
    R = PolynomialRing(GF(p**f), 'x') #R = F_q[x]
    x = R.gen()
    Q = R.quotient(x**e) #The ring of interest
    g_d = 1 #Begin inductive construction of g_d(x)
    for i in range(d):
        g_d *= (x + i)
    for y in Q:
        if g_d(y) != 0:
            return False
    return True

def least_d(p, f, e):
    """Check if g_d(x) vanishes on Q = F_{p^f}[x] / (x^e) for d = 1,2,... until it does, and return that d. Cap at d = 50."""
    d = 1
    while d &lt; 50:
        if is_zero(d, p, f, e):
            return d
        d += 1
    return -1

for p in [2, 3, 5]:
    l = [[least_d(p, f, e) for e in range(1, 10)] for f in range(1, 5)]
    print("p =", p)
    print(l)</code></pre>
      <p>
        As said, this code could surely be optimised drastically. For one, we
        could remember the output of $g_d(x)$ to easily compute $g_{d+1}(x)$.
        But more significantly, we are evaluating $g_d(x)$ at <em>every</em> $x$
        in $\mathbb{F}_{p^f}[\varepsilon] / (\varepsilon^e)$, which as
        we&rsquo;ll soon see is highly redundant. For now, though, this
        suffices. The results are simple enough:
      </p>
      <pre>
p: 2
[[2, 4, 6, 8, 10, 12, 14, 16, 18], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1]]
p: 3
[[3, 6, 9, 12, 15, 18, 21, 24, 27], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1]]
p: 5
[[5, 10, 15, 20, 25, 30, 35, 40, 45],
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1], 
 [-1, -1, -1, -1, -1, -1, -1, -1, -1]]</pre
      >
      <p>
        Let&rsquo;s verify our findings. Denote by $Q_{e,f}$ or $Q$ the ring
        $\mathbb{F}_{p^f}[\varepsilon] / (\varepsilon^e)$ to avoid spelling it
        out.
      </p>
      <p>
        <i>Proof:</i> We first claim that $g_d(x)$ never vanishes identically on
        $\mathbb{F}_{p^f}$ when $f > 1$. A fortiori, then, it doesn&rsquo;t
        vanish on $Q_{e,f}$ either. To prove the claim, notice that if $x \in
        \mathbb{F}_{p^f} \setminus \mathbb{F}_p$, then $x + n$ isn&rsquo;t in
        $\mathbb{F}_p$ either for any integer $n$. In particular, none of the $x
        + n$ are zero. Since $\mathbb{F}_{p^f}$ is a field, it has no zero
        divisors, so no such product can ever return zero.
      </p>
      <p>
        Now consider the ring $Q = Q_{e,1} = \mathbb{F}_p[\varepsilon] /
        (\varepsilon^e)$. I claim that $g_{d}(x)$ vanishes identically precisely
        when $d \geq pe$. We first prove that this bound is sufficient, i.e.
        that $g_{pe}(x)$ vanishes identically on $Q$. Any element $x$ in $Q$ is
        of the form $\varepsilon x' + n$ for some $n \in \{0,\ldots,p-1\}$. Upon
        writing out the product $g_{pe}(x)$ for such an element, we see the term
        $\varepsilon x'$ appear at least $e$ times, hence the product must
        vanish. Notice that, when applied to $x = \varepsilon$, it in fact
        suffices to take $d = p(e-1) + 1$ &mdash; a fact we&rsquo;ll use in a
        moment.
      </p>
      <p>
        The bound $d \geq pe$ is moreover necessary. In fact, I claim that
        $g_{pe - 1}(\varepsilon + 1) \neq 0$. To prove this, define $\Delta
        g_d(x)$ as the difference $g_d(x + 1) - g_d(x)$. Then it&rsquo;s easy to
        see that \[ \Delta g_d(x) = d g_{d-1}(x + 1) \text{.} \] Applied to $x =
        \varepsilon$, we use the vanishing of $g_d(\varepsilon)$ to find that \[
        g_{pe - 1}(\varepsilon + 1) = -g_{pe - 2}(\varepsilon + 1) \text{.} \]
        Continue this process until we get to conclude that \[ g_{pe -
        1}(\varepsilon + 1) = C g_{pe - p}(\varepsilon + 1) \] for some
        irrelevant but certainly nonzero constant $C$. Writing out the product,
        we find that \[ g_{pe - p}(\varepsilon + 1) = \prod_{n = 0}^{p - 1}
        (\varepsilon + n)^{e - 1} \text{.} \] Upon expanding this product, we
        find that almost all terms vanish, the only exception being the
        lowest-order term, which is $(p - 1)!\varepsilon$. As $p$ is a prime,
        $(p-1)!$ is nonzero modulo $p$. &#8718;
      </p>
      <p>
        <b>Theorem 6.</b> The polynomial $f(x) = x(x+1)(x+2) / 6$ preserves
        $\mathcal{O}_K$ if and only if the primes $2$ and $3$ split completely
        in $K$.
      </p>
      <p>
        <i>Proof:</i> $f(x)$ preserves $\mathcal{O}_K$ if and only if
        $x(x+1)(x+2)$ vanishes identically on $\mathcal{O}_K / (6)$. By the
        Chinese remainder theorem, this quotient splits as $\mathcal{O}_K / (2)
        \times \mathcal{O}_K / (3)$, which further splits into terms of the form
        $\mathcal{O}_K / (\mathfrak{P}^e)$. By Lemma 2 these terms are all of
        the form $\mathbb{F}_{p^f}[\varepsilon] / (\varepsilon^e)$ for $p = 2$
        or $3$. By the result above, we always want $f = 1$ and $e \leq 3/p$,
        which just means $e = 1$ both when $p = 2$ or $p = 3$. &#8718;
      </p>
      <p>
        <b>Example 7.</b> As in Example 4, consider a quadratic field $K =
        \mathbb{Q}(\sqrt{m})$, where $m$ is squarefree. The primes $2$ and $3$
        split completely if and only if $m \equiv 1$ mod $24$, and it&rsquo;s
        easy (but tedious) to check that $f(x)$ indeed preserves the ring of
        integers $\mathbb{Z}[(\sqrt{m} - 1)/2]$.
      </p>
      <p>
        <b>Example 8.</b> Here&rsquo;s a random cubic example. Let $K =
        \mathbb{Q}(\alpha)$ where $\alpha$ is a root of $x^3 + 35x - 60$. This
        is a degree-$3$ extension, <em>not</em> Galois, with discriminant $-5^2
        \times 2687$ and class group $C_{27}$. The ring of integers
        $\mathcal{O}_K$ can be expressed as \[ \mathcal{O}_K \cong
        \mathbb{Z}[\alpha, \beta] \qquad \text{with} \qquad \beta = \frac{1}{2}
        \alpha^2 - \frac{1}{2} \alpha \text{.} \] The primes $(2)$ and $(3)$
        split completely as \[ \begin{split} (2) &= (2,\alpha) \cdot (2, \beta +
        1) \cdot (2, \alpha + \beta + 1) \\ (3) &= (3,\alpha) \cdot (3,\alpha +
        1) \cdot (3,\alpha - 1) \end{split} \] The ring $\mathcal{O}_K$ is
        indeed closed under $f(x) = x(x+1)(x+2) / 6$; for instance, we can
        directly verify that \[ f(\alpha) = -5\alpha + \beta + 10 \] and \[
        f(\beta) = -128\alpha + 31\beta + 190 \text{.} \] There are surely many
        other examples that we could cook up.
      </p>
      <h3>A further extension: where things go wrong</h3>
      <p>
        The next polynomial to consider is $f(x) = x(x+1)(x+2)(x+3) / 24$. Here,
        a phenomenon arises that we haven&rsquo;t yet seen before: $24$ is no
        longer a product of distinct prime numbers, and so we find that \[
        \mathcal{O}_K / (24) \cong \mathcal{O}_K / (2)^3 \times \mathcal{O}_K /
        (3) \text{.} \] The quotient ring $\mathcal{O}_K / (2)^3$ falls outside
        the realm of Lemma 2, and the ideas in the proof break down. What can we
        do?
      </p>
      <p>
        At this point we might as well just go all out and consider the
        polynomial \[ f_d(x) = \frac{x(x+1)(x+2)\cdots(x+d-1)}{d!} =
        \frac{g_d(x)}{d!} \text{,} \] which obviously preserves integers for the
        same reason. To understand whether it preserves $\mathcal{O}_K$, we need
        to first understand the finite ring $\mathcal{O}_K / (p^N)$ for $N > 1$.
        Unfortunately for us, this turns out to be a hard question to answer
        directly: as it turns out, the local factors of a quotient such as
        $\mathcal{O}_K / (p^N)$ can be isomorphic to virtually every possible
        finite DVR of characteristic $p^N$, and there is no classification of
        such rings.
      </p>
      <p>
        Let&rsquo;s get the easy part out of the way. In the previous cases, we
        required all inertia to be trivial of the relevant prime numbers. I
        claim that this is still the case.
      </p>
      <p>
        <b>Lemma 9.</b> In order for $f_d(x)$ to preserve the ring of integers
        $\mathcal{O}_K$, we at least want all primes lying over the prime
        numbers $p \leq d$ to have trivial inertia.
      </p>
      <p>
        <i>Proof:</i> Observe that there is a surjective quotient map
        $\mathcal{O}_K / (p)^N \to \mathcal{O}_K / \mathfrak{P}$ for any prime
        $\mathfrak{P}$ lying over $(p)$. The target of this map is
        $\mathbb{F}_{p^f}$ where $f$ is the inertia degree of $\mathfrak{P}$.
        The quotient commutes with $g_d(x)$ and we had already seen that
        $g_d(x)$ never vanishes identically on $\mathbb{F}_{p^f}$ when $f > 1$.
        &#8718;
      </p>
      <p>
        There is one case where we can decisively figure out what the quotient
        ring $\mathcal{O}_K / (p^N)$ is like, namely in the case when
        there&rsquo;s no ramification either, i.e. when $(p)$ is totally split.
      </p>
      <p>
        <b>Lemma 10.</b> If $p$ is totally split in $K$, then $\mathcal{O}_K /
        (p^N)$ will just be a product of copies of $\mathbb{Z}/p^N\mathbb{Z}$.
      </p>
      <p>
        <i>Proof:</i> The proof uses a bit of algebraic geometry but it can
        probably be simplified quite a bit. Write $(p) = \mathfrak{P}_1 \cdots
        \mathfrak{P}_r$. By the Chinese remainder theorem it suffices to check
        that $\mathcal{O}_K / \mathfrak{P}^N \cong \mathbb{Z}/ p^N \mathbb{Z}$
        where $\mathfrak{P}$ is one of the maximal ideals lying over $p$. We
        first claim that $\mathcal{O}_K / \mathfrak{P}^N$ has order $p^N$. Using
        the short exact sequence of abelian groups \[ 0 \to \mathfrak{P}^{n} /
        \mathfrak{P}^{n+1} \to \mathcal{O}_K / \mathfrak{P}^{n+1} \to
        \mathcal{O}_K / \mathfrak{P}^{n} \to 0 \] it suffices to know that
        $\mathfrak{P}^{n} / \mathfrak{P}^{n+1}$ has order $p$. Notice that this
        quotient carries the structure of an $\mathcal{O}_K /
        \mathfrak{P}$-vector space. Generally speaking, if $I$ is an ideal in
        $R$ generated by a regular sequence, then there&rsquo;s an isomorphism
        $I^{n} / I^{n+1} \cong \operatorname{Sym}^n(I/I^2)$ which lets you
        deduce the dimension.<sup><a href="#footnotes">Note 1</a></sup> In our
        case, since $\operatorname{Spec} \mathcal{O}_K$ defines a smooth curve,
        $\mathfrak{P} / \mathfrak{P}^2$ must be $1$-dimensional, hence so is
        $\operatorname{Sym}^n(\mathfrak{P} / \mathfrak{P}^2)$ for every $n$.
      </p>
      <p>
        It remains to show that $\mathcal{O}_K / \mathfrak{P}^N$ has
        characteristic $p^N$. This would finish up the proof as it would
        establish that the underlying abelian group is
        $\mathbb{Z}/p^N\mathbb{Z}$, which admits only one ring structure. But
        this becomes obvious upon passing to $\mathfrak{P}$-completion, as $p$
        then acts as a uniformiser for $\mathfrak{P}$. &#8718;
      </p>
      <p>
        If there is ramification involved, then it is no longer clear what the
        residues modulo prime powers will be like. Nonetheless, we might attain
        some insights by restricting to a specific class of rings. Looking at
        Lemmas 2 and 10, there&rsquo;s one obvious family of rings that can
        surely arise as a quotient, namely the rings \[ Q_{e,N} =
        (\mathbb{Z}/p^N \mathbb{Z})[\varepsilon] / (\varepsilon^e) \text{.} \]
        It should be easy enough to figure out the vanishing behaviour of
        $g_d(x)$ on these rings. If $N = 1$, then we already know from Lemma 5
        that $d$ needs to be at least $pe$ for $g_d(x)$ to vanish identically.
        But what about larger $N$?
      </p>
      <p>
        I decided to take the Sage code written up earlier, and alter the base
        ring from $\mathbb{F}_{p^f}$ into $\mathbb{Z}/p^N \mathbb{Z}$, to see
        what would happen. But before doing so, let&rsquo;s first optimise the
        code using the following two improvements, which I&rsquo;ll state as
        lemmas.
      </p>
      <p>
        <b>Lemma 11.</b> If $g_d(x)$ vanishes identically on $Q_{e,N}$, then $d$
        needs to be at least $pe$.
      </p>
      <p>
        <i>Proof:</i> The proof is analogous to that of Lemma 9: simply observe
        that there&rsquo;s a projection $Q_{e,N} \to \mathbb{F}_p[\varepsilon] /
        (\varepsilon^e)$, and we know from Lemma 5 that $g_{d}(x)$ vanishes on
        the target only for $d \geq pe$. &#8718;
      </p>
      <p>
        <b>Lemma 12.</b> If $g_d(\varepsilon + n) = 0$ for $n = 0,\ldots,d$,
        then $g_d(x)$ vanishes on all of $Q_{e,N}$.
      </p>
      <p>
        <i>Proof:</i> I claim that $g_d(x),g_d(x+1),\ldots,g_d(x+d+1)$ are
        $\mathbb{Z}$-linearly dependent, hence if the first $d + 1$ polynomials
        vanish, then so does the $(d + 2)$-nd. By induction, then, all $g_d(x +
        n)$ for $n > d$ will vanish. To prove our claim, we induct on $d$.
        Recall the difference formula \[ \Delta g_d(x + n) = dg_d(x + n + 1) \]
        and apply the inductive hypothesis to find that $\Delta
        g_d(x),\ldots,\Delta g_d(x + d)$ are $\mathbb{Z}$-linearly dependent.
        Writing out the dependence, this proves the result.
      </p>
      <p>
        We note that it&rsquo;s unlikely that you can test $g_d(x)$ for
        significantly fewer $x$. For instance, if we take the ring
        $(\mathbb{Z}/4\mathbb{Z})[\varepsilon] / (\varepsilon^3)$, then one can
        check that $g_7(x + n) = 0$ for $n = 0,1,2,4,5,6,7$, yet \[ g_7(x + 3) =
        2\varepsilon^2 \text{.} \] Numerical evidence does, however, suggest
        that it suffices to check $g_d(x + n)$ for $n = 0,\ldots,\lfloor d /
        2\rfloor$. There may well be a hidden symmetry that we haven&rsquo;t yet
        exploited.
      </p>
      <p>
        There&rsquo;s a potential additional improvement, which increases
        performance by a factor $p$. For any $Q_{e,N}$, the minimum $d$ for
        which $g_d(x)$ vanishes always appears to be a multiple of $p$, so in
        our numerical analysis, we might as well skip all other values of $d$.
        However, I&rsquo;m unable to prove formally that this is the case, so we
        will not make use of it.
      </p>
      <p>Here&rsquo;s the code that we&rsquo;ll make use of.</p>
      <pre><code class="language-python">def is_zero(d, p, N, e):
    """Given a polynomial g_d(x) = x(x+1)...(x+d-1), test if g_d(x) vanishes everywhere on the ring Z_{p^N}[x] / (x^e)."""
    R = PolynomialRing(IntegerModRing(p**N), 'x')
    x = R.gen()
    Q = R.quotient(x**e) #The ring of interest
    a = Q.gen()
    g_d = 1
    for i in range(d):
        g_d *= (x + i) #Construct g_d
	check_range = min([p**N, d + 2])
    for n in range(check_range):
        y = a + n #We cannot write 'y = x + n' because Sage won't understand that we're evaluating f_d(y) in Q rather than in R
        if g_d(y) != 0:
            return False
    return True

def least_d(p, N, e):
    """Check if g_d(x) vanishes on Z_{p^N}[x] / (x^e) for d = 1,2,... until it does, and return that d. We've removed the cap as it's no longer needed."""
    d = p*e
    while True:
        if is_zero(d, p, N, e):
            return d
        d += 1 #You can probably change this to 'd += p'

for p in [2, 3, 5, 7]:
    print("p =", p)
    l = [[least_d(p, N, e) for e in range(1, 13)] for N in range(1, 9)]
    print(l)</code></pre>
      <p>The outcome is incredible:</p>
      <pre>
p = 2
[[2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24], 
 [4, 6, 8, 8, 12, 14, 16, 16, 20, 22, 24, 24], 
 [4, 8, 10, 12, 14, 16, 16, 16, 20, 24, 26, 28], 
 [6, 8, 12, 14, 16, 16, 18, 20, 22, 24, 28, 30], 
 [8, 10, 12, 16, 18, 20, 22, 24, 26, 28, 30, 32],
 [8, 12, 14, 16, 20, 22, 24, 24, 28, 30, 32, 32], 
 [8, 12, 14, 16, 20, 24, 26, 28, 30, 32, 32, 32], 
 [10, 14, 16, 18, 22, 24, 28, 30, 32, 32, 32, 32]]
p = 3
[[3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36], 
 [6, 9, 9, 15, 18, 18, 24, 27, 27, 33, 36, 36], 
 [9, 12, 15, 18, 21, 24, 27, 27, 27, 36, 39, 42], 
 [9, 15, 18, 18, 24, 27, 27, 30, 33, 36, 42, 45], 
 [12, 18, 21, 24, 27, 27, 27, 33, 36, 39, 45, 48], 
 [15, 18, 24, 27, 27, 30, 33, 36, 39, 42, 45, 51], 
 [18, 21, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54], 
 [18, 21, 27, 33, 36, 36, 42, 45, 45, 51, 54, 54]]
p = 5
[[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60], 
 [10, 15, 20, 25, 25, 35, 40, 45, 50, 50, 60, 65], 
 [15, 20, 25, 25, 30, 40, 45, 50, 50, 55, 65, 70], 
 [20, 25, 25, 30, 35, 45, 50, 50, 55, 60, 70, 75], 
 [25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80], 
 [25, 35, 40, 45, 50, 50, 60, 65, 70, 75, 75, 85], 
 [30, 40, 45, 50, 50, 55, 65, 70, 75, 75, 80, 90], 
 [35, 45, 50, 50, 55, 60, 70, 75, 75, 80, 85, 95]]
p = 7
[[7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84], 
 [14, 21, 28, 35, 42, 49, 49, 63, 70, 77, 84, 91], 
 [21, 28, 35, 42, 49, 49, 56, 70, 77, 84, 91, 98], 
 [28, 35, 42, 49, 49, 56, 63, 77, 84, 91, 98, 98], 
 [35, 42, 49, 49, 49, 63, 70, 84, 91, 98, 98, 98], 
 [42, 49, 49, 56, 63, 70, 77, 91, 98, 98, 105, 112],
 [49, 56, 63, 70, 77, 84, 91, 98, 105, 112, 119, 126], 
 [49, 63, 70, 77, 84, 91, 98, 98, 112, 119, 126, 133]]</pre
      >
      <p>
        We see what appears to be a linear increase in $e$ and $N$. The increase
        isn&rsquo;t perfect, however; there seem to be all sorts of little
        irregularities, which aren&rsquo;t easily explained. What&rsquo;s more
        is that the dependence on $e$ and $N$ is in fact not linear. This cannot
        be seen in the range outputted above, but it becomes obvious if we
        increase the range. The slopes in $e$ and $N$ slowly increase. How slow
        exactly? Logarithmic, perhaps? I was unable to tell.
      </p>
      <p>
        Looking only at the first vertical line (i.e. $e = 1$), we see an
        obvious pattern: as we increase $N$, the minimal $d$ increases in steps
        of $p$, but at the $p^k$-th such increase, there&rsquo;s a $k$-fold
        repetition of $d = p^k$. This pattern indeed continues without
        irregularity:
      </p>
      <p>
        <b>Lemma 13.</b> The minimal $d$ for which $g_d(x)$ vanishes on
        $\mathbb{Z}/p^N\mathbb{Z}$ equals the minimal $d$ for which $p^N$
        divides $d!$.
      </p>
      <p>
        <i>Proof:</i> Notice that $g_d(1) = d!$ so we need $p^N$ to divide $d!$
        in order for $g_d(1)$ to evaluate to $0$. The first such $d$ at which
        this happens will be divisible by $p$, since otherwise, the $p$-adic
        valuation of $(d-1)!$ and $d!$ would be the same, contradicting
        minimality.<sup><a href="#footnotes">Note 2</a></sup> In fact, this
        specific $g_d(x)$ will be uniformly zero on $\mathbb{Z} /
        p^N\mathbb{Z}$. To see this, expand the product $g_d(n)$ for $n > 1$ and
        consider its residue modulo $p$. &#8718;
      </p>
      <p>
        <b>Corollary 14.</b> Let $K$ be a number field. Write $d! = p_1^{N_1}
        \cdots p_r^{N_r}$. In the hypothetical scenario that the local summands
        of the $\mathcal{O}_K / p_i^{N_i}$ are all isomorphic to some
        $Q_{e,N_i}$, then $f_d(x)$ will be well-defined on $\mathcal{O}_K$ if
        and only if all $p \leq d$ are totally split.
      </p>
      <p>
        <i>Proof:</i> Obviously, the $p_i^{N_i}$ divide $d!$, so by Lemma 13, we
        know that $g_d(x)$ vanishes on $Q_{N,1} = \mathbb{Z}/p^N \mathbb{Z}$.
        However, I claim that it will not vanish on at least some $Q_{N,2}$.
        This can be seen by noting that $g_d(\varepsilon) = (d-1)! \varepsilon$,
        which is nonzero on some factor $Q_{N,2}$ for a $p$ dividing $d$.
        &#8718;
      </p>
      <p>
        It&rsquo;s tempting to say that this result holds regardless of the
        precise algebraic nature of the relevant quotient rings. As such we wind
        up with the following conjecture:
      </p>
      <p>
        <b>Conjecture 15.</b> The polynomial $f_d(x)$ preserves $\mathcal{O}_K$
        if and only if all primes less than or equal to $d$ split completely in
        $K$.
      </p>
      <p>
        Notice that if this conjecture is true then the minimal $d$ for which
        $f_d(x)$ preserves $\mathcal{O}_K$ will always be a prime number.
      </p>
      <p>
        I have attempted to find counterexamples to Conjecture 15 using
        brute-force computations in Sage, but I have been unable to find any.
        Nonetheless, I would hesitate to say for sure that the conjecture is
        true. One evidence in the opposite direction is that if, instead of
        $f_d(x)$, we consider an integer multiple of $f_d(x)$, then one can find
        counterexamples to the conjecture.
      </p>
      <p>
        <b>Example 16.</b> If we take the field $K = \mathbb{Q}(\alpha)$ with
        $\alpha^3 + 567\alpha + 168 = 0$ then the polynomial $f_5(x)$
        doesn&rsquo;t preserve $\mathcal{O}_K$, but $3 f_5(x)$ does. In fact, in
        this field, the prime $(3)$ is totally ramified, splitting as a cube
        $(3,\alpha)^3$.
      </p>
      <p>
        <b>Example 17.</b> Consider the polynomial $g_d(x) / \prod_{p \leq d}
        p$. Then the arguments of Theorem 6 extend readily to imply that
        $f_d(x)$ preserves $\mathcal{O}_K$ if and only if every prime
        $\mathfrak{P}$ lying over a prime number $p \leq d$ has trivial inertia
        and has ramification index $e \leq d/p$.
      </p>
      <h3>Aside on integer-valued polynomials</h3>
      <p>
        We started out this post with the question whether $x(x+1) / 2$
        preserves rings of integers in a given number field. After answering
        this question, we observed that the product can be extended, so that we
        can ask the same question for the integer-valued polynomial \[ f_d(x) =
        \frac{x(x+1)\cdots(x+d-1)}{d!} \text{.} \] At that point, it is obvious
        that we can ask this question for <em>any</em> rational polynomial that
        preserves $\mathbb{Z}$. This, in turn, raises the question whether we
        can classify such polynomials.
      </p>
      <p>
        <b>Lemma 18.</b> Let $f(x)$ be a rational polynomial which preserves
        integers. Then $f(x)$ is an integer linear combination of the $f_d(x)$
        defined above.
      </p>
      <p>
        <i>Proof:</i> The proof method resembles that of Lemma 12. We induct on
        the degree $n$ of the polynomial $f(x)$. If $f(x)$ is a polynomial of
        degree $n$, then define $\Delta f(x) = f(x+1) - f(x)$. Notice that this
        is a polynomial of degree $n - 1$. Notice moreover that $\Delta$ is
        almost injective, in that if $\Delta f(x) = \Delta g(x)$, then $f(x)$
        and $g(x)$ differ by a constant.
      </p>
      <p>
        If $f(x)$ preserves integers, then so does $\Delta f(x)$. Moreover, note
        that \[ \begin{split} \Delta f_d(x) &= f_{d-1}(x+1) \\ &= f_{d-1}(x) +
        f_{d-2}(x) + \cdots + f_1(x) + 1 \end{split} \] The induction hypothesis
        shows that this map yields a surjection onto the space of
        integer-preserving polynomials of degree $n - 1$, so we are done.
        &#8718;
      </p>
      <p>
        Another obvious question to ask is whether rings of integers admit any
        exotic rational integer-valued polynomials. That is, given a number
        field $K$, are there any polynomials $f(x) \in K[x]$ preserving
        $\mathcal{O}_K$ that do not come from any of the $f_d(x)$ defined above?
      </p>
      <p>
        <b>Example 19.</b> Consider the field $K = \mathbb{Q}(i)$. The prime $2$
        is ramified, splitting as $(2) = (1 + i)^2$. From Corollary 3 we know
        that $x(x+1) / 2$ doesn&rsquo;t preserve $\mathcal{O}_K$, but from
        $x(x+1) / (1 + i)$ does.
      </p>
      <p>
        In fact, this example readily extends to any number field $K$ for which
        $\mathcal{O}_K$ is a PID. However, all polynomials obtained in this way
        are still morally close to the $f_d(x)$: they are a still an
        &lsquo;integer&rsquo; multiple of the $f_d(x)$ &mdash; it&rsquo;s just
        that the integers are in $\mathcal{O}_K$ instead of $\mathbb{Z}$. This
        is to be expected:
      </p>
      <p>
        <b>Lemma 20.</b> Let $K$ be a number field, and let $f(x) \in K[x]$
        preserve $\mathcal{O}_K$. Then $f(x)$ is an $\mathcal{O}_K$-linear
        combination of the $f_d(x)$.
      </p>
      <p>
        <i>Proof:</i> We claim that, upon multiplying $f(x)$ by a suitable
        scalar in $\mathcal{O}_K$, it will land in $\mathbb{Q}[x]$, after which
        the result follows by Lemma 18. First multiply by a scalar to clear the
        denominators of the coefficient of $f(x)$. Next, I claim that for every
        coefficient $c \in \mathcal{O}_K$ of $f(x)$, one can find a scalar $c'
        \in \mathcal{O}_K$ such that $c \cdot c' \in \mathbb{Z}$. If $K$ is a
        Galois field, one can take $c'$ to be the product of the Galois
        conjugates of $c$. In other cases, $c$ can be written as the root of
        some monic polynomial $x^n + a_{n-1} x^{n-1} + \cdots + a_0$. Rewrite
        this into \[ c \cdot (c^{n-1} + a_{n-1}c^{n-2} + a_{n-2} c^{n-3} +
        \cdots + a_1 c) = -a_0 \text{,} \] so that we can put \[ c' = a_{n-1} +
        a_{n-2} c^{n-2} + + \cdots + a_1 c \text{.} \] This proves the desired
        result. &#8718;
      </p>
      <h3>Remark on the literature</h3>
      <p>
        While writing up this post, I came to find out that much of this has
        been studied on some form or other. In fact, there&rsquo;s an entire
        book on this particular subject, aptly titled
        <i>Integer-Valued Polynomials</i>, by Cahen&ndash;Chabert.
      </p>
      <p>
        Although I haven&rsquo;t read the book, it contains a historical
        introduction which is available online. The results therein overlap
        substantially with what I&rsquo;ve written in this post. Notably, Lemma
        18 dates all the way back to a 1915 paper of George P&oacute;lya, titled
        <i>&Uuml;ber ganzwertige ganze Funktionen</i>.
      </p>
      <p>
        The table of contents of the book suggests many other interesting
        avenues to study: the ring structure on the set of integer-valued
        polynomials; integer-valued rational functions; and multivariate
        integer-valued polynomials. So if the reader wants to learn more, he now
        knows where to look.
      </p>
      <h3 id="footnotes">Footnotes</h3>
      <ol>
        <li>
          In such generality, this is a hard theorem, but I would expect that
          there&rsquo;s an easy proof when restricting attention to curves,
          which is all we need.
        </li>
        <li>
          Presumably, a comparable argument can be made to show that the minimal
          $d$ for which $g_d(x)$ vanishes on $Q_{e,N}$ is divisible by $p$. This
          would allow for a roughly $p$-fold speedup in the algorithm I
          described.
        </li>
      </ol>
    </main>
    <footer>
      <hr>
      <p>Last updated: April 10, 2022</p>
    </footer>
  </body>
</html>
