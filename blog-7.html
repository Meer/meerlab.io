<!DOCTYPE html>
<html lang="en-US">
  <head>
    <!--Metadata-->
    <meta charset="utf-8">
    <meta name="author" content="Jeroen van der Meer">
    <title>Reverse-engineering MAT-files Part I</title>
    <!--Stylesheet-->
    <link rel="stylesheet" href="style.css">
    <!--Mobile-friendliness-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <header>
      <nav>
        <p>
          <a href="blog-6.html">&larr;&nbsp;Previous</a>&nbsp;&verbar;&nbsp;<a
            href="index.html"
            >Home</a
          >&nbsp;&verbar;&nbsp;<a href="blog-8.html">Next&nbsp;&rarr;</a>
        </p>
        <hr>
      </nav>
    </header>
    <main>
      <h2>
        Reverse-engineering MAT-files<br>Part 1: Introduction and toy examples
      </h2>
      <p>
        MATLAB is a programming language primarily known for its emphasis on
        matrices and arrays as its primary data structure. Owing to this, as
        well as to its renowned ease of use for non-programmers, MATLAB is
        widely used in the engineering domain.
      </p>
      <p>
        At my company, there has been a recent push away from MATLAB in favour
        of open-source alternatives. A lot of work has been put in migrating the
        existing MATLAB infrastructure. In the process, many hurdles have been
        encountered. One such hurdle is the way that MATLAB stores data, and
        it&rsquo;s the hurdle that I&rsquo;d like to talk about today.
      </p>
      <p>
        Since MATLAB is the lingua franca among engineers, much of the data that
        we work with gets stored and exchanged in so-called <b>MAT-files</b>. A
        MAT-file is MATLAB&rsquo;s way of storing data. When working with MATLAB
        you can, at any point, save the variables that you currently have in
        memory into a MAT-file. You or your friend can then, at a later point in
        time, open this MAT-file to reconstruct your past workspace. Owing to
        MATLAB&rsquo;s excellent dedication to backward compatibility, reading
        MAT-files is extremely robust, and most likely the MAT-files you created
        twenty years ago can still be read today.
      </p>
      <p>
        The problem with MAT-files is that it relies on a very proprietary
        format. As such, it is very difficult to parse a MAT-file without a
        MATLAB license. As my company aims to move away from MATLAB, it is vital
        that we are able to convert the contents of our MAT-file into a format
        that other languages can handle.
      </p>
      <p>
        To the best of my knowledge, open-source options for handling MAT-files
        are limited. There are definitely some tools here and there, but they
        are usually limited in scope. Notably, although simple arrays and
        structs can typically be parsed, most of these tools fall short of
        parsing objects. This is because MAT-files encode objects in a rather
        peculiar manner which is difficult to reverse-engineer.
      </p>
      <p>
        I felt that it would be a fun challenge to try and reverse-engineer
        MATLAB&rsquo;s data format. In the next couple of posts I&rsquo;ll share
        with you what I have learned so far.
      </p>
      <p>
        Note: I will be focussing entirely on the v7.3 version of MAT-files,
        which is MATLAB&rsquo;s most modern and most proprietary format. That
        said, some of the key concepts will carry over to older versions.
      </p>
      <h3>What is HDF5?</h3>
      <p>
        MAT v7.3 builds on the HDF5 format, so it&rsquo;d serve us well to dwell
        on this first. I&rsquo;ll keep this as brief as I can.
      </p>
      <p>
        <b>HDF5</b> is a file format designed for storing large amounts of data.
        An HDF5 file is basically a filesystem in a file, but with a few crucial
        differences:
      </p>
      <ul>
        <li>
          Directories are called <b>groups</b> and files are called
          <b>datasets</b>.
        </li>
        <li>
          Datasets can&rsquo;t contain arbitrary data; rather, they are
          (possibly multidimensional) rectangular arrays containing homogeneous
          data.
        </li>
        <li>
          Groups and datasets can have metadata in the form of
          <b>attributes</b>.
        </li>
        <li>
          Unlike an ordinary filesystem, there can be multiple paths leading to
          the same dataset. This can be helpful for bookkeeping. Suppose you
          want to do multiple version of an experiment over multiple days. A
          traditional filesystem would force you to either sort your data
          &lsquo;by day&rsquo; or sort it &lsquo;by experiment version&rsquo;.
          Non-unique paths in HDF5 allow you to do both at once. We will not use
          this feature.
        </li>
      </ul>
      <h3>Some toy MAT-files</h3>
      <p>We start with a couple of toy examples to get our feet wet.</p>
      <pre>
>> x = 9001;
>> save('double.mat', 'x', '-v7.3');</pre
      >
      <p>
        After creating a variable <code>x</code>, we use the
        <code>save</code> function to save this variable in a MAT-file. We
        explicitly specify that we want to store it in the
        <code>v7.3</code> format.
      </p>
      <p>
        We are going to use the Julia wrapper
        <a href="https://juliaio.github.io/HDF5.jl/">HDF5.jl</a> to analyse our
        MAT-files.
      </p>
      <pre>
julia> using HDF5
julia> h = h5open("double.mat")</pre
      >
      <p>Let's see the output.</p>
      <pre>
🗂️ HDF5.File: (read-only) double.mat
└─ 🔢 x
    └─ 🏷️ MATLAB_class</pre
      >
      <p>
        Our HDF5 🗂️&nbsp;file has a single 🔢&nbsp;dataset <code>/x</code>,
        which in turn has an 🏷️&nbsp;attribute <code>MATLAB_class</code>. We
        read our the contents and the attribute as follows.
      </p>
      <pre>
julia> read_attribute(h["x"], "MATLAB_class")
"double"</pre
      >
      <pre>
julia> read(h["x"])
1×1 Matrix{Float64}:
 9001.0</pre
      >
      <p>
        So there you have it, the value we were looking for. In typical MATLAB
        fashion, it is comfortably wrapped in a 1-by-1 matrix.
      </p>
      <p>Here&rsquo;s another toy example.</p>
      <pre>
>> x = 'Foobar';
>> save('char.mat', 'x', '-v7.3');</pre
      >
      <p>We open it with HDF5.jl and obtain the following:</p>
      <pre>
julia> h = h5open("char.mat")
🗂️ HDF5.File: (read-only) char.mat
└─ 🔢 x
    ├─ 🏷️ MATLAB_class
    └─ 🏷️ MATLAB_int_decode</pre
      >
      <p>We read out <code>/x</code> in the same way as before:</p>
      <pre><code>julia> read(h["x"])
1×6 Matrix{UInt16}:
 0x0046  0x006f  0x006f  0x0062  0x0061  0x0072</code></pre>
      <p>
        These numbers are simply the ASCII representation of
        <code>'Foobar'</code>. As for the 🏷️&nbsp;attributes, the
        <code>MATLAB_class</code> attribute reads out as <code>"char"</code>,
        but not we also find a mysterious
        <code>MATLAB_int_decode</code> attribute, which returns <code>2</code>.
      </p>
      <p>
        Now for one more toy example. We ramp up the complexity ever so
        slightly.
      </p>
      <pre>
x = struct();
x.Foo = 9001;
x.Bar = 9002;
save('struct.mat', 'x', '-v7.3');</pre
      >
      <pre>
julia> h = h5open("struct.mat")
🗂️ HDF5.File: (read-only) struct.mat
├─ 📂 #refs#
│  └─ 🔢 a
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_empty
└─ 📂 x
   ├─ 🏷️ MATLAB_class
   ├─ 🏷️ MATLAB_fields
   ├─ 🔢 Bar
   │  ├─ 🏷️ H5PATH
   │  └─ 🏷️ MATLAB_class
   └─ 🔢 Foo
      ├─ 🏷️ H5PATH
      └─ 🏷️ MATLAB_class</pre
      >
      <p>
        That already looks a lot scarier, but we easily recognise our fields
        <code>Foo</code> and <code>Bar</code>. The 📂 indicates that
        <code>/x</code> is now a group rather than a dataset. In turn,
        <code>/x</code> contains the 🔢&nbsp;datasets <code>/x/Foo</code> and
        <code>/x/Bar</code> which inevitably have the numbers
        <code>9001</code> and <code>9002</code> hidden within them.
      </p>
      <pre>
julia> read(h["x"]["Foo"])
1×1 Matrix{Float64}:
 9001.0</pre
      >
      <pre>
julia> read(h["x"]["Bar"])
1×1 Matrix{Float64}:
 9002.0</pre
      >
      <p>
        Let&rsquo;s also read out some of the attributes to get a better feel
        for the metadata.
      </p>
      <pre>
julia> read_attribute(h["x"], "MATLAB_class")
"struct"</pre
      >
      <pre>
julia> read_attribute(h["x"], "MATLAB_fields")
2-element Vector{Vector{String}}:
 ["F", "o", "o"]
 ["B", "a", "r"]</pre
      >
      <p>What about those <code>H5PATH</code> attributes?</p>
      <pre>
julia> read_attribute(h["x"]["Foo"], "H5PATH")
"/x"</pre
      >
      <pre>
julia> read_attribute(h["x"]["Bar"], "H5PATH")
"/x"</pre
      >
      <p>Some rather redundant metadata. Not to be concerned about.</p>
      <p>
        For completeness, I also want to briefly check out the other dataset,
        📂&nbsp;<code>/#refs#</code>. It cannot possibly contain anything of
        value since we already have all the information needed to reconstruct
        our object, but as it happens, 📂&nbsp;<code>/#refs#</code> will wind up
        becoming very important later on.
      </p>
      <pre>
julia> read(h["#refs#"]["a"])
2-element Vector{UInt64}:
 0x0000000000000000
 0x0000000000000000</pre
      >
      <pre>
julia> read_attribute(h["#refs#"]["a"], "MATLAB_class")
"canonical empty"</pre
      >
      <pre>
julia> read_attribute(h["#refs#"]["a"], "MATLAB_empty")
0x01</pre
      >
      <p>
        A bunch of numbers of class <code>"canonical empty"</code>. There is
        nothing that we can make out from this, frankly, but it&rsquo;s good to
        know it&rsquo;s there.
      </p>
      <p>The last toy example I want to consider is a simple cell array.</p>
      <pre>
>> x = cell(2, 1);
>> x{1} = 9001;
>> x{2} = 'Foobar';
>> save('cell.mat', 'x', '-v7.3');</pre
      >
      <pre>
julia> h = h5open("cell.mat")
🗂️ HDF5.File: (read-only) cell.mat
├─ 📂 #refs#
│  ├─ 🔢 a
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 🔢 b
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  └─ 🔢 c
│     ├─ 🏷️ H5PATH
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_int_decode
└─ 🔢 x
   └─ 🏷️ MATLAB_class</pre
      >
      <p>
        We see a bit more stuff in 📂&nbsp;<code>#refs#</code>, but let&rsquo;s
        first look at 🔢&nbsp;<code>x</code>.
      </p>
      <pre>
julia> read_attribute(h["x"], "MATLAB_class")
"cell"</pre
      >
      <pre>
julia> read(h["x"])
2×1 Matrix{HDF5.Reference}:
 HDF5.Reference(HDF5.API.hobj_ref_t(0x0000000000000ab0))
 HDF5.Reference(HDF5.API.hobj_ref_t(0x0000000000000bc8))</pre
      >
      <p>
        We see that <code>/x</code> is indeed a <code>"cell"</code>. Rather than
        containing the contents directly, <code>/x</code> contains two
        references to other locations within the file. This is not too
        surprising, really, since cell arrays can take on arbitrary values.
      </p>
      <p>
        Julia&rsquo;s HDF5.jl provides convenient indexing syntax for following
        an HDF5 reference:
      </p>
      <pre>julia> r = read(h["x"]);</pre>
      <pre>
julia> h[r[1]]
🔢 HDF5.Dataset: /#refs#/b (file: cell.mat xfer_mode: 0)
├─ 🏷️ H5PATH
└─ 🏷️ MATLAB_class</pre
      >
      <pre>
julia> h[r[2]]
🔢 HDF5.Dataset: /#refs#/c (file: cell.mat xfer_mode: 0)
├─ 🏷️ H5PATH
├─ 🏷️ MATLAB_class
└─ 🏷️ MATLAB_int_decode</pre
      >
      <p>
        We see that the references in <code>/x</code> point to
        <code>/#refs#/b</code> and <code>/#refs#/c</code>. We read out those
        datasets to find the values we stored:
      </p>
      <pre>
julia> read(h[r[1]])
1×1 Matrix{Float64}:
 9001.0</pre
      >
      <pre>
julia> read(h[r[2]])
1×6 Matrix{UInt16}:
 0x0046  0x006f  0x006f  0x0062  0x0061  0x0072</pre
      >
      <p>
        For what it&rsquo;s worth, just as with <code>struct.mat</code>,
        <code>/#refs#/a</code> has <code>MATLAB_class</code>
        <code>"canonical empty"</code> and contains a bunch of zeros.
      </p>
      <h3 id="firstclass">Our first classes</h3>
      <p>
        We&rsquo;ve seen a couple of very simple objects, and so far, life has
        been good. The objects are stored in the most straightforward possible
        manner using datasets with appropriate labels. Granted, we&rsquo;ve
        found some junk here and there, but we&rsquo;ve been able to safely
        ignore it.
      </p>
      <p>
        Now I want to talk about classes.<sup
          ><a href="#footnotes">Note 1</a></sup
        >
        MATLAB supports object-oriented programming with the so-called
        <b>MATLAB Class Object System</b> or <b>MCOS</b>. Let&rsquo;s create a
        very simple class by putting the following contents in a file called
        <code>MyClass.m</code>.
      </p>
      <pre>
classdef MyClass
    properties
        Foo
        Bar
    end
    methods
        function obj = MyClass(foo, bar)
            obj.Foo = foo;
            obj.Bar = bar;
        end
    end
end</pre
      >
      <p>
        What we&rsquo;ve done is create a class <code>MyClass</code> with two
        fields, <code>Foo</code> and <code>Bar</code>. I have not specified what
        values these fields must contain, meaning that they can take on whatever
        value we want for now. Thus, our class effectively behaves like a
        struct. For convenience, I also created a default constructor under
        <code>methods</code>.
      </p>
      <pre>
>> x = MyClass(9001, 9002);
>> save('myclass.mat', 'x', '-v7.3');</pre
      >
      <p>We open our MAT-file as per usual:</p>
      <pre>
julia> h = h5open("myclass.mat")
🗂️ HDF5.File: (read-only) myclass.mat
├─ 📂 #refs#
│  ├─ 🔢 a
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 🔢 b
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 c
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 d
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_int_decode
│  ├─ 🔢 e
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 f
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 g
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  └─ 🔢 h
│     ├─ 🏷️ H5PATH
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_empty
├─ 📂 #subsystem#
│  └─ 🔢 MCOS
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_object_decode
└─ 🔢 x
   ├─ 🏷️ MATLAB_class
   └─ 🏷️ MATLAB_object_decode</pre
      >
      <p>
        Wow, that&rsquo;s a whole lot of junk! But the important part is that we
        have our <code>/x</code>, right? Perhaps, but notice is that, unlike in
        our struct, <code>/x</code> is not a 📂&nbsp;group, but a
        🔢&nbsp;dataset. In particular, <code>/x</code> cannot, and does not,
        have subfolders <code>/x/Foo</code> and <code>/x/Bar</code>. This could
        imply that <code>/x</code> might not have the information that
        we&rsquo;re looking for.
      </p>
      <p>
        Indeed when we read out <code>/x</code> we&rsquo;re greeted with the
        following:
      </p>
      <pre>
julia> read(h["x"])
6×1 Matrix{UInt32}:
 0xdd000000
 0x00000002
 0x00000001
 0x00000001
 0x00000001
 0x00000001</pre
      >
      <pre>
julia> read_attribute(h["x"], "MATLAB_class")
"MyClass"</pre
      >
      <pre>
julia> read_attribute(h["x"], "MATLAB_object_decode")
3</pre
      >
      <p>
        The <code>MATLAB_class</code> attribute confirms that we&rsquo;re
        definitely looking at the right object. But the actual content of
        <code>/x</code> is seemingly meaningless.
      </p>
      <p>
        As it turns out, the actual values of <code>x</code> isn&rsquo;t stored
        here; rather, it&rsquo;s hidden within the <code>#refs#</code> group.
      </p>
      <table class="generaltable" style="white-space: nowrap">
        <tr>
          <th>Dataset</th>
          <th><code>MATLAB_class</code></th>
          <th>Value<br></th>
        </tr>
        <tr>
          <td><code>/#refs#/a</code></td>
          <td><code>"canonical empty"</code></td>
          <td>
            <pre>
2-element Vector{UInt64}:
 0x0000000000000000
 0x0000000000000000</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/b</code></td>
          <td><code>"uint8"</code></td>
          <td>
            <pre>
192×1 Matrix{UInt8}:
 0x03
 0x00
 ⋮
 0x00
 0x00</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/c</code></td>
          <td><code>"double"</code></td>
          <td>
            <pre>
1×1 Matrix{Float64}:
 9001.0</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/d</code></td>
          <td><code>"char"</code></td>
          <td>
            <pre>
1×6 Matrix{UInt16}:
 0x0046  0x006f  0x006f  0x0062  0x0061  0x0072</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/e</code></td>
          <td><code>"int32"</code></td>
          <td>
            <pre>
2×1 Matrix{Int32}:
 0
 0</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/f</code></td>
          <td><code>"cell"</code></td>
          <td>
            <pre>
2×1 Matrix{HDF5.Reference}:
 HDF5.Reference(HDF5.API.hobj_ref_t(0x00000000000016b0))
 HDF5.Reference(HDF5.API.hobj_ref_t(0x00000000000017e8))</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/g</code></td>
          <td><code>"struct"</code></td>
          <td>
            <pre>
2-element Vector{UInt64}:
 0x0000000000000001
 0x0000000000000000</pre
            >
          </td>
        </tr>
        <tr>
          <td><code>/#refs#/h</code></td>
          <td><code>"struct"</code></td>
          <td>
            <pre>
2-element Vector{UInt64}:
 0x0000000000000001
 0x0000000000000000</pre
            >
          </td>
        </tr>
      </table>
      <p>
        Most of it is unreadable, but under <code>/#refs#/c</code> and
        <code>/#refs#/d</code> we unambiguously recognise the values
        <code>9001</code> and <code>'Foobar'</code>.
      </p>
      <p>
        So we know where to find the actual values stored in the fields of our
        class. But how do we couple these values to the associated class? How do
        we find the correct structure? For instance, suppose we had done the
        following:
      </p>
      <pre>
classdef MyClass
    properties
        Foo
    end
    methods
        function obj = MyClass(foo)
            obj.Foo = foo;
        end
    end
end</pre
      >
      <pre>
>> s = struct();
>> s.Field1 = 9001;
>> s.Field2 = 'Foobar';
>> x = MyClass(s);
>> save('myclass.mat', 'x', '-v7.3');</pre
      >
      <p>
        We would still be able to find the values <code>9001</code> and
        <code>'Foobar'</code> somewhere in our MAT-file, but we&rsquo;re clearly
        dealing with a different object, and we ought to be able to
        differentiate between them and reconstruct the entire structure.
      </p>
      <p>
        In principle, one could reasonably fear that the MAT-file simply lacks
        the necessary information. After all, when MATLAB loads our object,
        MATLAB could simply use the contents of <code>MyClass.m</code> to infer
        the actual structure of the class, and in this way it could match the
        values in <code>myclass.mat</code> with the appropriate fields.
      </p>
      <p>
        Luckily for us, this isn&rsquo;t the case. MAT-files are essentially
        self-contained in that the entire structure of the class can be
        reverse-engineered based on the available metadata. The crucial
        component is <code>/#refs#/b</code>, which is stored as an unstructured
        stream of bytes, but which in fact contains all the information needed
        for our reconstruction.
      </p>
      <p>Below is a hexdump of <code>/#refs#/b</code>:</p>
      <pre>
00000000: 0300 0000 0300 0000 3800 0000 5800 0000  ........8...X...
00000010: 5800 0000 8800 0000 b000 0000 c000 0000  X...............
00000020: 0000 0000 0000 0000 466f 6f00 4261 7200  ........Foo.Bar.
00000030: 4d79 436c 6173 7300 0000 0000 0000 0000  MyClass.........
00000040: 0000 0000 0000 0000 0000 0000 0300 0000  ................
00000050: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000060: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000070: 0100 0000 0000 0000 0000 0000 0000 0000  ................
00000080: 0100 0000 0100 0000 0000 0000 0000 0000  ................
00000090: 0200 0000 0100 0000 0100 0000 0000 0000  ................
000000a0: 0200 0000 0100 0000 0100 0000 0000 0000  ................
000000b0: 0000 0000 0000 0000 0000 0000 0000 0000  ................</pre
      >
      <p>Let&rsquo;s look at the results.</p>
      <ul>
        <li>
          <code>/#refs#/b</code> starts off with a couple of numbers separated
          by zeros. Actually, these are little-endian 32-bit integers. The
          numbers seem rather arbitrary. It may be observed that the eighth
          number (<code>0xc0</code>) is exactly equal to the number of bytes in
          <code>/#refs#/b</code>.
        </li>
        <li>
          After exactly 40 (<code>0x28</code>) bytes, we see the names of both
          our fields <em>and</em> of our class stored in ASCII, separated by a
          single null byte.
        </li>
        <li>
          Beyond this point, we find a load of zeros interspersed with a few
          other numbers.
        </li>
      </ul>
      <p>
        The third part of <code>/#refs#/b</code>, the part with all the zeros,
        is what actually encodes the structure of our object. Decoding it is
        rather challenging, and will be the main topic of
        <a href="blog-8.html">part 2</a>.
      </p>
      <h3>Appendix: The parts of <code>myclass.mat</code> that we skipped</h3>
      <p>
        We have ignored a few components of <code>myclass.mat</code> that may
        seem to contain interesting information. As it turns out, not much
        happens in these components, but for completeness I&rsquo;ll briefly
        summarise what&rsquo;s inside of them.
      </p>
      <ul>
        <li>
          <code>/#refs#/f</code> was seen to contain two references. These
          references in fact point to <code>/#refs#/g</code> and
          <code>/#refs#/h</code>, respectively, which in turn have no real
          content.
        </li>
        <li>
          The 📂&nbsp;<code>#subsystem#</code> group has a single dataset 🔢
          <code>MCOS</code> (&lsquo;MATLAB Class Object System&rsquo;). The
          <code>MATLAB_class</code> attribute is <code>FileWrapper__</code> and
          the <code>MATLAB_object_decode</code> attribute agains returns
          <code>3</code>. The dataset contains six references, and they point to
          <code>/#refs#/b</code>, <code>/#refs#/a</code>,
          <code>/#refs#/c</code>, <code>/#refs#/d</code>,
          <code>/#refs#/e</code> and <code>/#refs#/f</code>, in that order.
        </li>
      </ul>
    </main>
    <h3 id="footnotes">Footnotes</h3>
    <ol>
      <li>
        Technically, MATLAB has <em>two</em> kinds of classes. We will focus on
        the more modern of the two, recognised by the
        <code>classdef</code> syntax. The other kind of class is more archaic,
        and is recognised by its <code>@</code>-directories. That is, an
        old-style class <code>Foo</code> has its code in a folder named
        <code>@Foo</code>. Old-style classes are stored in the same way as
        structs.
      </li>
    </ol>
    <footer>
      <hr>
      <p>Last updated: July 20, 2024</p>
    </footer>
  </body>
</html>
