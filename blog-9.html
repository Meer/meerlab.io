<!DOCTYPE html>
<html lang="en-US">
  <head>
    <!--Metadata-->
    <meta charset="utf-8">
    <meta name="author" content="Jeroen van der Meer">
    <title>Reverse-engineering MAT-files Part 3</title>
    <!--Stylesheet-->
    <link rel="stylesheet" href="style.css">
    <!--Mobile-friendliness-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <header>
      <nav>
        <p>
          <a href="blog-8.html">&larr;&nbsp;Previous</a>&nbsp;&verbar;&nbsp;<a
            href="index.html"
            >Home</a
          >&nbsp;&verbar;&nbsp;<a href="blog-10.html">Next&nbsp;&rarr;</a>
        </p>
        <hr>
      </nav>
    </header>
    <main>
      <h2>
        Reverse-engineering MAT-files<br>Part 3: Classes with attributes
      </h2>
      <p>
        In <a href="blog-8.html">part 2</a> of this series we discovered how
        simple classes are stored in MAT v7.3. We did so by looking at a few
        very simple classes and examining how our MAT-file changes when we, say,
        alter the class structure a bit, or when we add more objects of the same
        class.
      </p>
      <p>
        The simple examples we considered merely scratch the surface of what is
        possible in MATLAB&rsquo;s class system. MATLAB supports many features
        typical of an object-oriented programming language, such as abstract
        classes, inheritance, private and protected attributes, operator
        overloading, and so on and so forth.
      </p>
      <p>
        Luckily for us, <em>most</em> of these features do not seem to influence
        the way that MATLAB stores the object in a MAT-file. There are, however,
        a few exceptions to this, and the goal of this post is to highlight
        these exceptions.
      </p>
      <p>
        More concretely, we will look at classes exhibiting the following
        features:
      </p>
      <ul>
        <li><a href="#default-values">Default values</a></li>
        <li><a href="#constant">Constant and dependent values</a></li>
        <li><a href="#typed">Properties with specified type and/or size</a></li>
      </ul>
      <h3 id="preliminary">Preliminary: Arrays of classes</h3>
      <p>Before looking at classes, we need to talk about arrays.</p>
      <p>
        Arrays of primitive objects such as doubles and chars are stored in a
        straightforward way. Consider the following example.
      </p>
      <pre>
>> x = rand(1, 2, 3);
>> save('out.mat', 'x', '-v7.3');</pre
      >
      <p>Our MAT-file consists of a single 🔢&nbsp;dataset <code>x</code>:</p>
      <pre>
julia> read(h["x"])
1×2×3 Array{Float64, 3}:
[:, :, 1] =
 0.815771  0.804843

[:, :, 2] =
 0.360404  0.913814

[:, :, 3] =
 0.288316  0.165002</pre
      >
      <p>
        The only exception to this logic is if our array has size zero in some
        dimension. In MATLAB, an &lsquo;empty&rsquo; array of size 3 by 0 is
        different from an array of size 0 by 3. Yet we evidently cannot
        distinguish them by their content, since there is no content!
        Here&rsquo;s what MATLAB does instead.
      </p>
      <pre>
>> x = rand(9001, 0, 9002);
>> save('out.mat', 'x', '-v7.3');</pre
      >
      <pre>
julia> read(h["x"])
3-element Vector{UInt64}:
 0x0000000000002329
 0x0000000000000000
 0x000000000000232a</pre
      >
      <p>
        If at least one of the sizes of an array is zero, MATLAB does a
        &lsquo;fallback&rsquo; by saving the <em>size</em> of the array in the
        form of 64-bit integers.<sup><a href="#footnotes">Note 1</a></sup> The
        type that the elements would have had if there were elements can be
        inferred from the <code>MATLAB_class</code> attribute. This allows us to
        distinguish, for instance, a <code>3×0</code> character array from a
        <code>3×0</code> array of doubles.
      </p>
      <p>
        Now what would happen if we take say a <code>1×2×3</code> array of
        <em>objects</em>?
      </p>
      <p>
        We define the following simple class which we&rsquo;ll use throughout
        this post.
      </p>
      <pre>
classdef MyClass
   properties
       Foo
   end
   methods
       function obj = MyClass(x)
           obj.Foo = x;
       end
   end
end</pre
      >
      <p>Now consider the following:</p>
      <pre>
x = [MyClass(9001) MyClass(9002) MyClass(9003) MyClass(9004) MyClass(9005) MyClass(9006)];
x = reshape(x, 1, 2, 3);
save('out.mat', 'x', '-v7.3');</pre
      >
      <p>
        So far, our classes had always been represented by a length-six vector
        of integers, of which the last two encode
        <span style="color: #fa0">object ID</span> and
        <span style="color: #f0f">class ID</span>. So should we now expect a
        <code>6×1×2×3</code> array of integers instead?
      </p>
      <pre>
julia> read(h["x"])
12×1 Matrix{UInt32}:
 0xdd000000
 <span style="color: #0fb">0x00000003
 0x00000001
 0x00000002
 0x00000003</span>
 <span style="color: #fa0">0x00000001
 0x00000002
 0x00000003
 0x00000004
 0x00000005 
 0x00000006</span>
 <span style="color: #f0f">0x00000001</span></pre>
      <p>
        This is altogether different. For convenience, I&rsquo;ve indicated in
        <span style="color: #0fb">mint</span> a new component which we
        haven&rsquo;t elaborated on before. This component indicates the
        <em>size</em> of the array of objects. Specifically, it tells us that
        we&rsquo;re dealing with a
        <code><span style="color: #0fb">3</span></code
        >-dimensional array of size
        <code><span style="color: #0fb">1×2×3</span></code
        >. After that, the <span style="color: #fa0">object ID</span> of each
        object in the array is listed in column-major order, and finally, we
        specify the <span style="color: #f0f">class ID</span> of the objects.
      </p>
      <p>
        Until this point, objects have always been represented with six
        integers, of which the first four seemed fixed. In hindsight,
        you&rsquo;ll be able to recognise that the second, third and fourth
        integer simply told us that the object was a
        <code><span style="color: #0fb">2</span></code
        >-dimensional array of size
        <code><span style="color: #0fb">1×1</span></code
        >.
      </p>
      <p>
        Before moving on to the next section, I leave you with one example. Can
        you guess how MATLAB encodes the following? For a hint, see
        <a href="#footnotes">Note 2</a>.
      </p>
      <pre>
>> x = MyClass.empty; % This returns a 0×0 array of objects of type MyClass
>> save('out.mat', 'x', '-v7.3');</pre
      >
      <h3 id="default-values">Default values</h3>
      <p>Consider the following class.</p>
      <pre>
classdef MyClassWithDefault
   properties
       Bar = MyClass(9001)
   end
end</pre
      >
      <p>
        As you&rsquo;d probably expect, if I create a variable
        <code>x = MyClassWithDefault()</code>, the field <code>Bar</code> will
        obtain the default value <code>MyClass(9001)</code>.
      </p>
      <p>
        Let&rsquo;s take a look at the MAT-file that we get upon saving
        <code>x</code>.
      </p>
      <pre>
🗂️ HDF5.File: (read-only) out.mat
├─ 📂 #refs#
│  ├─ 🔢 a
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 🔢 b
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 c
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 d
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 e
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 f
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 📂 g
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🔢 Bar
│  │     ├─ 🏷️ H5PATH
│  │     └─ 🏷️ MATLAB_class
│  └─ 🔢 h
│     ├─ 🏷️ H5PATH
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_empty
├─ 📂 #subsystem#
│  └─ 🔢 MCOS
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_object_decode
└─ 🔢 x
   ├─ 🏷️ MATLAB_class
   └─ 🏷️ MATLAB_object_decode</pre
      >
    </main>
    <p>
      Interestingly, <code>/#refs#/g</code> is now a 📂&nbsp;group rather than a
      🔢&nbsp;dataset. Here&rsquo;s what&rsquo;s inside <code>/#refs#</code>:
    </p>
    <table class="generaltable" style="white-space: nowrap">
      <tr>
        <th>Dataset</th>
        <th><code>MATLAB_class</code></th>
        <th>Value<br></th>
      </tr>
      <tr>
        <td><code>/#refs#/a</code></td>
        <td><code>"canonical empty"</code></td>
        <td>
          <pre>
0x0000000000000000
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/b</code></td>
        <td><code>"uint8"</code></td>
        <td>
          <code>256×1 Matrix{UInt8}</code><br>
          (Content omitted)
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/c</code></td>
        <td><code>"double"</code></td>
        <td>
          <pre>9001.0</pre>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/d</code></td>
        <td><code>"int32"</code></td>
        <td>
          <pre>
0
0
0</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/e</code></td>
        <td><code>"cell"</code></td>
        <td>
          Ref to <code>/#refs#/f</code><br>
          Ref to <code>/#refs#/g</code><br>
          Ref to <code>/#refs#/h</code>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/f</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
0x0000000000000001
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/g</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>"Bar" => [
 0xdd000000
 <span style="color: #0fb">0x00000002
 0x00000001
 0x00000001</span>
 <span style="color: #fa0">0x00000002</span>
 <span style="color: #f0f">0x00000002</span>
]</pre>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/h</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
0x0000000000000001
0x0000000000000000</pre
          >
        </td>
      </tr>
    </table>
    <p>
      <code>/#refs#/g/Bar</code> encodes the default value of the field
      <code>Bar</code> of the first class. As we learned in
      <a href="blog-8.html">part 2</a>, the six <code>UInt32</code>&rsquo;s are
      MATLAB&rsquo;s way of telling you that this default value is itself an
      object, of <span style="color: #fa0">object ID</span>
      <span style="color: #fa0">2</span> and
      <span style="color: #f0f">class ID</span>
      <span style="color: #f0f">2</span>. In turn, the value of the field
      <code>Foo</code> of this object is stored in <code>/#refs#/c</code>.
    </p>
    <p>
      As you may expect, <code>/#refs#/b</code> is what ties things together.
    </p>
    <pre>00000000: <span style="color: #f44">0300 0000 0400 0000 5000 0000 8000 0000</span>  ........P.......
00000010: <span style="color: #f44">8000 0000 c800 0000 e800 0000 0001 0000</span>  ................
00000020: 0000 0000 0000 0000 <span style="color: #4f4">4d79 436c 6173 7357</span>  ........MyClassW
00000030: <span style="color: #4f4">6974 6844 6566 6175 6c74 0042 6172 0046</span>  ithDefault.Bar.F
00000040: <span style="color: #4f4">6f6f 004d 7943 6c61 7373 0000 0000 0000</span>  oo.MyClass......
00000050: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000060: <span style="color: #44f">0000 0000 0100 0000 0000 0000 0000 0000</span>  ................
00000070: <span style="color: #44f">0000 0000 0400 0000 0000 0000 0000 0000</span>  ................
00000080: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000090: 0000 0000 0000 0000 <span style="color: #0ff">0100 0000 0000 0000</span>  ................
000000a0: <span style="color: #0ff">0000 0000 0000 0000 0100 0000 0200 0000</span>  ................
000000b0: <span style="color: #0ff">0200 0000 0000 0000 0000 0000 0000 0000</span>  ................
000000c0: <span style="color: #0ff">0200 0000 0100 0000</span> 0000 0000 0000 0000  ................
000000d0: <span style="color: #ff0">0000 0000 0000 0000 0100 0000 0300 0000</span>  ................
000000e0: <span style="color: #ff0">0100 0000 0000 0000</span> 0000 0000 0000 0000  ................
000000f0: 0000 0000 0000 0000 0000 0000 0000 0000  ................</pre>
    <p>
      The <span style="color: #f44">red</span> component still lists all the
      sizes that we identified in part 2, the
      <span style="color: #4f4">green</span> component lists of the names of the
      classes and fields, and the
      <span style="color: #44f">blue</span> component lists which of these names
      are classes. But something interesting happens in the
      <span style="color: #0ff">cyan</span> and
      <span style="color: #ff0">yellow</span> components. Using the shorthand
      that we introduced <a href="blog-8.html#structural-part">here</a>,
      it&rsquo;s tempting to represent the components as follows:
    </p>
    <pre><span style="color: #0ff">100011 200021</span> 0000 <span style="color: #ff0">1-310</span></pre>
    <p>
      This is not correct. The <span style="color: #0ff">cyan</span> and
      <span style="color: #ff0">yellow</span> components are separated by
      <em>two</em>
      rather than four zeros, so the third and fourth zero aren&rsquo;t
      separators. Instead, they are &lsquo;empty&rsquo; triplets which
      specifically serve to indicate that the value at this object is default.
      So our shorthand representation should be
    </p>
    <pre><span style="color: #0ff">100011 200021</span> 00 <span style="color: #ff0">0-0 1-310</span></pre>
    <p>Now let&rsquo;s turn to the next MAT-file.</p>
    <pre>
>> x = MyClassWithDefault();
>> x.Bar.Foo = 9002;
>> save('out.mat', 'x', '-v7.3');</pre
    >
    <p>
      We again start with an object <code>x = MyClassWithDefault()</code>, but
      this time, we change the value of <code>x.Bar.Foo</code> from the default
      <code>9001</code> to <code>9002</code>. Then you may expect to get the
      same MAT-file, but with a <code>9002</code> in <code>/#refs#/c</code>.
    </p>
    <p>If only things were that easy.</p>
    <pre>
🗂️ HDF5.File: (read-only) out.mat
├─ 📂 #refs#
│  ├─ 🔢 a
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 🔢 b
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 c
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 d
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 e
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 f
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 g
│  │  ├─ 🏷️ H5PATH
│  │  └─ 🏷️ MATLAB_class
│  ├─ 🔢 h
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  ├─ 🔢 i
│  │  ├─ 🏷️ H5PATH
│  │  ├─ 🏷️ MATLAB_class
│  │  └─ 🏷️ MATLAB_empty
│  └─ 📂 j
│     ├─ 🏷️ H5PATH
│     ├─ 🏷️ MATLAB_class
│     └─ 🔢 Bar
│        ├─ 🏷️ H5PATH
│        └─ 🏷️ MATLAB_class
├─ 📂 #subsystem#
│  └─ 🔢 MCOS
│     ├─ 🏷️ MATLAB_class
│     └─ 🏷️ MATLAB_object_decode
└─ 🔢 x
   ├─ 🏷️ MATLAB_class
   └─ 🏷️ MATLAB_object_decode</pre
    >
    <p>
      There are now <em>ten</em> members of <code>/#refs#</code> rather than
      eight. Inside, we find the following:
    </p>
    <table class="generaltable" style="white-space: nowrap">
      <tr>
        <th>Dataset</th>
        <th><code>MATLAB_class</code></th>
        <th>Value<br></th>
      </tr>
      <tr>
        <td><code>/#refs#/a</code></td>
        <td><code>"canonical empty"</code></td>
        <td>
          <pre>
0x0000000000000000
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/b</code></td>
        <td><code>"uint8"</code></td>
        <td>
          <code>312×1 Matrix{UInt8}</code><br>
          (Content omitted)
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/c</code></td>
        <td><code>"double"</code></td>
        <td>
          <pre>9002.0</pre>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/d</code></td>
        <td><code>"uint32"</code></td>
        <td>
          <pre>
0xdd000000
<span style="color: #0fb">0x00000002
0x00000001
0x00000001</span>
<span style="color: #fa0">0x00000002</span>
<span style="color: #f0f">0x00000001</span></pre>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/e</code></td>
        <td><code>"double"</code></td>
        <td><pre>9001.0</pre></td>
      </tr>
      <tr>
        <td><code>/#refs#/f</code></td>
        <td><code>"int32"</code></td>
        <td>
          <pre>
0
0
0</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/g</code></td>
        <td><code>"cell"</code></td>
        <td>
          Ref to <code>/#refs#/h</code><br>
          Ref to <code>/#refs#/i</code><br>
          Ref to <code>/#refs#/j</code>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/h</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
0x0000000000000001
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/i</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
0x0000000000000001
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/j</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>"Bar" => [
 0xdd000000
 <span style="color: #0fb">0x00000002
 0x00000001
 0x00000001</span>
 <span style="color: #fa0">0x00000003</span>
 <span style="color: #f0f">0x00000001</span>
]</pre>
        </td>
      </tr>
    </table>
    <p>
      Take a good look at the values and compare them to the previous table.
    </p>
    <ul>
      <li>
        For whatever reason, MATLAB has now reversed the ordering of the
        classes. <code>MyClass</code> comes first, and then comes
        <code>MyClassWithDefault</code>. We will also see this in the
        <span style="color: #4f4">green</span> component of
        <code>/#refs#/b</code> in a moment. Luckily for us, we don&rsquo;t need
        to know why MATLAB orders classes in a certain way, as it does not
        impact our ability to parse the file.
      </li>
      <li>
        The default value of <code>Bar</code> hasn&rsquo;t been replaced;
        rather, the replaced value has simply been added to a new dataset in
        <code>/#refs#</code>.
      </li>
      <li>
        In fact, our MAT-file now contains <em>three</em> objects rather than
        two:
        <ol>
          <li>
            The object <code>x</code>, with
            <span style="color: #fa0">object ID</span>
            <span style="color: #fa0">1</span> and
            <span style="color: #f0f">class ID</span>
            <span style="color: #f0f">2</span>, sits in <code>/x</code> (not
            shown above).
          </li>
          <li>
            The object <code>x.Bar</code>, with
            <span style="color: #fa0">object ID</span>
            <span style="color: #fa0">2</span> and
            <span style="color: #f0f">class ID</span>
            <span style="color: #f0f">1</span>, sits in <code>/#refs#/d</code>.
          </li>
          <li>
            The default value of <code>x.Bar</code>, with
            <span style="color: #fa0">object ID</span>
            <span style="color: #fa0">3</span> and
            <span style="color: #f0f">class ID</span>
            <span style="color: #f0f">1</span>, sits in
            <code>/#refs#/j</code> under the key <code>"Bar"</code>. This object
            never actually gets loaded; it&rsquo;s just hanging around in our
            file for no good reason.
          </li>
        </ol>
      </li>
    </ul>

    <p>Here&rsquo;s <code>/#refs#/b</code>:</p>
    <pre>00000000: <span style="color: #f44">0300 0000 0400 0000 5000 0000 8000 0000</span>  ........P.......
00000010: <span style="color: #f44">8000 0000 e000 0000 1801 0000 3801 0000</span>  ............8...
00000020: 0000 0000 0000 0000 <span style="color: #4f4">4261 7200 466f 6f00</span>  ........Bar.Foo.
00000030: <span style="color: #4f4">4d79 436c 6173 7300 4d79 436c 6173 7357</span>  MyClass.MyClassW
00000040: <span style="color: #4f4">6974 6844 6566 6175 6c74 0000 0000 0000</span>  ithDefault......
00000050: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000060: <span style="color: #44f">0000 0000 0300 0000 0000 0000 0000 0000</span>  ................
00000070: <span style="color: #44f">0000 0000 0400 0000 0000 0000 0000 0000</span>  ................
00000080: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000090: 0000 0000 0000 0000 <span style="color: #0ff">0200 0000 0000 0000</span>  ................
000000a0: <span style="color: #0ff">0000 0000 0000 0000 0100 0000 0300 0000</span>  ................
000000b0: <span style="color: #0ff">0100 0000 0000 0000 0000 0000 0000 0000</span>  ................
000000c0: <span style="color: #0ff">0200 0000 0100 0000 0100 0000 0000 0000</span>  ................
000000d0: <span style="color: #0ff">0000 0000 0000 0000 0300 0000 0200 0000</span>  ................
000000e0: 0000 0000 0000 0000 <span style="color: #ff0">0100 0000 0100 0000</span>  ................
000000f0: <span style="color: #ff0">0100 0000 0100 0000 0100 0000 0200 0000</span>  ................
00000100: <span style="color: #ff0">0100 0000 0000 0000 0100 0000 0200 0000</span>  ................
00000110: <span style="color: #ff0">0100 0000 0200 0000</span> 0000 0000 0000 0000  ................
00000120: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000130: 0000 0000 0000 0000                      ........</pre>
    <p>
      Next, I&rsquo;ll show you the <span style="color: #0ff">cyan</span> and
      <span style="color: #ff0">yellow</span> components in compact form
      juxtaposed against the previous MAT-file.
    </p>
    <pre>
Previous: <span style="color: #0ff">100011 200021       </span> 00 <span style="color: #ff0">0-0 1-310</span> 
New:      <span style="color: #0ff">200013 100021 100032</span> 00 <span style="color: #ff0">1-111 1-210 1-212</span></pre>
    <p>
      There are now three <span style="color: #0ff">sextets</span> and
      <span style="color: #ff0">triplets</span>. From left to right, the three
      <span style="color: #0ff">sextets</span> correspond to:
    </p>
    <ol>
      <li>The object <code>x</code>.</li>
      <li>The object <code>x.Bar</code>.</li>
      <li>
        The default value of <code>x.Bar</code>, which happens to be an object.
      </li>
    </ol>
    <p>
      The corresponding three
      <span style="color: #ff0">triplet</span> correspond to:
    </p>
    <ol>
      <li>The value of <code>x.Bar</code>.</li>
      <li>The value of <code>x.Bar.Foo</code>.</li>
      <li>
        The value of the <code>Foo</code> field of the default value of
        <code>x.Bar</code>.
      </li>
    </ol>
    <p>
      For the sake of clarity we briefly look at one more example. Consider the
      following scenario.
    </p>
    <pre>
classdef MyClassWithManyDefaults
   properties
       Bar1 = 9001
       Bar2 = 9002
       Bar3 = 9003
   end
end</pre
    >
    <pre>
>> x = MyClassWithManyDefaults();
>> x.Bar3 = 9004;
>> save('out.mat', 'x', '-v7.3');</pre
    >
    <p>
      When we do this, we find all the default values of
      <code>MyClassWithManyDefaults</code> in the 📂&nbsp;group
      <code>/#refs#/g</code>, and we additionally find the value
      <code>9004</code> stored in <code>/#refs#/c</code>. Perhaps surprisingly,
      the <span style="color: #0ff">cyan</span> and
      <span style="color: #ff0">yellow</span> components are as follows.
    </p>
    <pre>
<span style="color: #0ff">100011</span> 00 <span style="color: #ff0">1-110</span></pre>
    <p>
      Since none of our objects have a default value, there&rsquo;s no
      <code><span style="color: #ff0">0-0</span></code> &mdash; yet MATLAB
      leaves implicit the fact that <code>x</code> has three fields rather than
      one; instead, this has to be inferred from the default values as stored in
      <code>/#refs#/g</code>.
    </p>
    <p>
      I&rsquo;ll leave you with one more example to think about for yourself.
      The following example is perfectly legal in MATLAB.
    </p>
    <pre>
classdef MyClassWithRandomDefault
   properties
       Bar = rand(3, 3)
   end
end</pre
    >
    <p>
      This class has a default value for its field <code>Bar</code>, but the
      default value isn&rsquo;t really fixed. So suppose I initialise an object
      of type <code>MyClassWithRandomDefault</code>, and I replace its field
      <code>Bar</code> with a freshly generated 3-by-3 matrix of random numbers.
      Will MATLAB consider it the default value or will it store both the
      initial value and the replaced one?
    </p>
    <h3 id="constant">Constant and dependent values</h3>
    <p>
      We can be rather brief here. Constant and dependent values don&rsquo;t get
      stored.
    </p>
    <p>To illustrate, consider the following example.</p>
    <pre>
classdef MyClassWithConstant
   properties
       Foo1
   end
   properties (Constant)
       Foo2 = MyClass(9002)
   end
end</pre
    >
    <pre>
>> x = MyCLassWithConstant();
>> x.Foo1 = 9001;
>> save('out.mat', 'x', '-v7.3');</pre
    >
    <p>
      Looking through <code>/#refs#</code>, we see no reference to the value
      <code>9002</code>. Moreover, when we look at <code>/#refs#/b</code>, we
      find the following.
    </p>
    <pre>00000000: <span style="color: #f44">0300 0000 0200 0000 4800 0000 6800 0000</span>  ........H...h...
00000010: <span style="color: #f44">6800 0000 9800 0000 b000 0000 c000 0000</span>  h...............
00000020: 0000 0000 0000 0000 <span style="color: #4f4">466f 6f31 004d 7943</span>  ........Foo1.MyC
00000030: <span style="color: #4f4">6c61 7373 5769 7468 436f 6e73 7461 6e74</span>  lassWithConstant
00000040: <span style="color: #4f4">0000 0000 0000 0000 0000 0000 0000 0000</span>  ................
00000050: 0000 0000 0000 0000 <span style="color: #44f">0000 0000 0200 0000</span>  ................
00000060: <span style="color: #44f">0000 0000 0000 0000</span> 0000 0000 0000 0000  ................
00000070: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000080: <span style="color: #0ff">0100 0000 0000 0000 0000 0000 0000 0000</span>  ................
00000090: <span style="color: #0ff">0100 0000 0100 0000</span> 0000 0000 0000 0000  ................
000000a0: <span style="color: #ff0">0100 0000 0100 0000 0100 0000 0000 0000</span>  ................
000000b0: 0000 0000 0000 0000 0000 0000 0000 0000  ................</pre>
    <p>
      From the perspective of <code>/#refs#/b</code>,
      <code>MyClassWithConstant</code> has only a single field,
      <code>Foo1</code>; the constant field <code>Foo2</code> isn&rsquo;t
      referenced anywhere in the file.
    </p>
    <p>Constant values disappear even in more complex cases:</p>
    <pre>
classdef MyClassWithConstant2
   properties (Constant)
       Foo = {MyClass(9001), 'Hello, world!', rand(100, 100)};
   end
end</pre
    >
    <p>
      When saving an object of class <code>MyClassWithConstant2</code>, the
      object will show up but it will appear to have no content whatsoever.
    </p>
    <p>
      Our discussion on constant properties applies verbatim to dependent
      properties. They neither get referenced nor stored in the MAT-file.
    </p>
    <h3 id="typed">Properties with specified type and/or size</h3>
    <p>
      When defining a class, MATLAB allows you to specify the type and size of
      the properties. For example:
    </p>
    <pre>
classdef MyClassWithFixedSizeDouble
   properties
       Bar (1, 2, :, 4) double
   end
end</pre
    >
    <p>
      The field <code>Bar</code> of this class <em>must</em> be a 4-dimensional
      array of chars, where the size of the array along the first, second, and
      fourth dimension is restricted to 1, 2, and 4, respectively.
    </p>
    <pre>
x = MyClassWithFixedSizeDouble();
x.Bar = rand(1, 2, 3, 4);
save('out.mat', 'x', '-v7.3');</pre
    >
    <p>
      Brief aside: When we run <code>MyClassWithFixedSizeDouble()</code> without
      any arguments, MATLAB calls <code>double()</code> without any arguments
      <code>1×2×3×4</code> times, giving you a zero array in <code>x.Bar</code>.
    </p>
    <p>Here&rsquo;s what we find under <code>/#refs</code>:</p>
    <table class="generaltable" style="white-space: nowrap">
      <tr>
        <th>Dataset</th>
        <th><code>MATLAB_class</code></th>
        <th>Value<br></th>
      </tr>
      <tr>
        <td><code>/#refs#/a</code></td>
        <td><code>"canonical empty"</code></td>
        <td>
          <pre>
0x0000000000000000
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/b</code></td>
        <td><code>"uint8"</code></td>
        <td>
          <code>192×1 Matrix{UInt8}</code><br>
          (Content omitted)
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/c</code></td>
        <td><code>"double"</code></td>
        <td>
          <code>1×2×3×4 Array{Float64, 4}</code><br>
          (Content omitted)
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/d</code></td>
        <td><code>"int32"</code></td>
        <td>
          <pre>
0
0</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/e</code></td>
        <td><code>"cell"</code></td>
        <td>
          Ref to <code>/#refs#/f</code><br>
          Ref to <code>/#refs#/g</code>
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/f</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
0x0000000000000001
0x0000000000000000</pre
          >
        </td>
      </tr>
      <tr>
        <td><code>/#refs#/g</code></td>
        <td><code>"struct"</code></td>
        <td>
          <pre>
"Bar" => [
 0x0000000000000001
 0x0000000000000002
 0x0000000000000000
 0x0000000000000004
]</pre
          >
        </td>
      </tr>
    </table>
    <p>
      Our table looks very similar to the objects with
      <a href="#default-values">default values</a> we studied earlier. Indeed,
      this is not a coincidence. The general pattern is as follows.
    </p>
    <p>
      When a field, say <code>Bar</code>, is constrained to be of type
      <code>D</code> and of size <code>n1×n2×n3×...</code>, MATLAB will give
      <code>Bar</code> a default value. This default value is an array of size
      <code>n1×n2×n3×...</code> where each element is <code>D()</code>.<sup
        ><a href="#footnotes">Note 3</a></sup
      >
      If the size is unconstrained in some direction, the size of the default
      value in that direction will be zero. But, as we discussed in the
      <a href="#preliminary">preliminary</a>, arrays that are of size zero in
      some direction cannot be represented by their content because there is no
      content, and so in that case, MATLAB falls back to representing the array
      in terms of its size. This is why <code>/#refs#/g/Bar</code> has value
      <code>[1, 2, 0, 4]</code>.
    </p>
    <p>
      So to be clear: If we had required the field <code>Bar</code> to have size
      <code>1×2×3×4</code>, <code>/#refs#/g/Bar</code> would contain a
      4-dimensional array consisting of 24 zeros, and <em>not</em>
      <code>[1, 2, 3, 4]</code>.
    </p>
    <p>Two exceptional cases should be considered.</p>
    <ul>
      <li>
        What if we specify the type but not the size? In this case, the default
        constructor will have size <code>0×0</code>.
      </li>
      <li>
        What if we specify the size but not the type? In this case, the default
        constructor will consist of doubles.
      </li>
    </ul>
    <p>
      I should point out that there are other ways to implicitly specify the
      type of a property. For instance, consider the following.
    </p>
    <pre>
classdef MyClassWithSmallNumber
   properties
      Bar {mustBeLessThanOrEqual(Bar, 2)}
   end
end</pre
    >
    <p>
      It is implied by <code>{mustBeLessThanOrEqual(Bar, 2)}</code> that
      <code>Bar</code> must be a number. As such,
      <code>MyClassWithSmallNumber</code> will have a <code>0×0</code> default
      constructor in the same way that it would have if we had declared
      <code>Bar</code> to be <code>double</code>.
    </p>
    <p>
      Before closing off, here&rsquo;s the last example for you to think about.
    </p>
    <pre>
classdef MyClassWithClass
   properties
       Bar MyClass
   end
   methods
       function obj = MyClassWithClass(x)
           obj.Bar = x;
       end
   end
end</pre
    >
    <pre>
>> x = MyClassWithClass(MyClass(9001));
>> save('out.mat', 'x', '-v7.3');</pre
    >
    <p>
      The default value of the field <code>Bar</code> resides in
      <code>/#refs#/i/Bar</code>. Based on the information in this post, can you
      guess what it looks like?
    </p>
    <p>
      In the <a href="blog-10.html">next post</a>, we&rsquo;ll use our knowledge
      to analyse how some common built-in classes are saved. We will
      particularly spend some time on strings, as they will reveal new phenomena
      that we haven&rsquo;t yet encountered.
    </p>
    <h3 id="footnotes">Footnotes</h3>
    <ol>
      <li>
        We&rsquo;ve seen that <code>/#refs#/a</code>, if it exists, is of type
        <code>"canonical empty"</code> and contains the data
        <pre>
0x0000000000000000
0x0000000000000000</pre
        >
        We now understand that this literally how MATLAB encodes an array of
        size
        <code>0×0</code>.
      </li>
      <li>Hint: No fallback is required for objects.</li>
      <li>
        <code>D</code> requires a zero-argument default constructor for this to
        work. If it doesn&rsquo;t have one, and you call <code>C()</code>,
        MATLAB will give you an error about not being able to construct an
        object of class <code>D</code>.
      </li>
    </ol>
    <footer>
      <hr>
      <p>Last updated: August 10, 2024</p>
    </footer>
  </body>
</html>
